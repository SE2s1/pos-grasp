package kdg.pos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by overvelj on 14/11/2016.
 */
public class Sale {
    private List<SalesLineItem> slis;
    private boolean isComplete;
    private Payment payment;

    public Sale() {
        this.slis = new ArrayList<SalesLineItem>();
        this.isComplete = false;
    }

    public List<SalesLineItem> getSalesLineItems(){
        return slis;
    }

    public void makeSalesLineItem(ProductDescription itemDesc, int qty) {
        slis.add(new SalesLineItem(itemDesc,qty));
    }

    public Payment getPayment() {
        return payment;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(){
        isComplete = true;
    }

    public double getTotal() {
        double total=0;
        for(SalesLineItem sli : slis){
            total += sli.getSubTotal();
        }
        return total;
    }

    public void makePayment(double i) {
        payment = new Payment(i);
    }
}
