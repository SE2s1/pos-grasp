package kdg.pos;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by overvelj on 14/11/2016.
 */
public class POSTest {

	private Register r;

     // Use @BeforeEach method to set up test data.
     // Refrain from making test data in your domain classes!
    @BeforeEach
    public void setUp()  {
        Store s = new Store();
        r = s.getRegister();
        ProductCatalog catalog = r.getCatalog();
        ProductDescription pd1 = new ProductDescription(1, "One", 1.2);
        ProductDescription pd2 = new ProductDescription(2, "Two", 3.54);
        catalog.addProduct(pd1);
        catalog.addProduct(pd2);
    }


     // annotate all test methods
    @Test
    public void createNewSaleTest()  {
        // 1. call code to be tested
        r.makeNewSale();
       // 2. test if the system has fulfilled its responsibilities correctly
        // What should you test?
        //   - postconditions of the operation contract
        //   - returned data
        //   - correct handling of error conditions.
        assertNotNull(r.getSale());
        assertNotNull(r.getSale().getSalesLineItems());
        assertFalse(r.getSale().isComplete());
    }

    /**
     * @BeforeEach is also executed before this test
     * With refreshed test data, the results of earlier tests are erased.
     * Do not make any suppositions about the order of test methods
     * (you can even configure the framework to execute tests in parallel)
     */
    @Test
    public void addItemTest() {
        r.makeNewSale(); // step 1
        r.enterItem(1,10); // step 2
        assertEquals(1,
                r.getSale().getSalesLineItems().get(0).getProductDescription().getProductId());
    }

    @Test
    public void endSaleTest(){
        r.makeNewSale();
        r.enterItem(1,10);
        r.endSale();
        assertTrue(r.getSale().isComplete());
    }

    @Test
    public void getTotalTest() {
        r.makeNewSale();
        r.enterItem(1,10);
        r.enterItem(2,5);
        r.endSale();
        assertEquals((10*1.2)+(5*3.54),r.getTotal(),0);
    }

    @Test
    public void makePaymentTest() {
        r.makeNewSale();
        r.enterItem(1,10);
        r.enterItem(2,5);
        r.endSale();
        assertEquals(0,r.getStore().countSales(),0);
        r.makePayment(30);
        assertEquals(30,r.getSale().getPayment().getAmount(),0);
        assertEquals(1,r.getStore().countSales(),0);
    }


}
