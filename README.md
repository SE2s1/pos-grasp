#POS process sale 

Implementatie of the **process sale** use case in het POS business case in Applying UML and patterns (Larman).

## branches

### master (gradle)

Base gradle project with JUnit tests

### junit4

Old version with JUnit 4 tests

### theo-filter

Old version with JUnit 4 tests using streams/lambda, and a Categorie enum (droge voeding...)

### cucumber

Gradle project with Cucumber tests

### dataTable

Gradle project with Cucumber tests plus test data in Cucumber datatables
